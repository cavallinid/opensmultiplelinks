#!/usr/local/bin/python3.7.6
"""Includes"""
from lib import command


def main():
    save_pdf = True
    # Instance webbrowser manager
    get_browser = command.GetBrowser('lista.txt')
    # Execute
    get_browser.set_remove_path('https://pollini-staging.factory.madeinevolve.com/backend/ecommerce/order/shipped/edit/')
    get_browser.execute(save_pdf)


if __name__ == "__main__":
    main()
