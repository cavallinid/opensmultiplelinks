"""Includes"""
import platform
import re
import webbrowser
import pdfkit


class GetBrowser:
    _debug = False
    _browsers = [
        {'name': 'firefox', 'service': ['mozilla', 'firefox', 'netscape']},
        {'name': 'safari', 'service': 'safari'},
        {'name': 'internet explorer/edge', 'service': 'windows-default'},
    ]
    _urls = []
    _file_name = ''
    _remove_path = ''

    def __init__(self, file_name, only_chrome=None):
        # Set links file name
        self._file_name = file_name

        system = platform.system()
        if system == 'Darwin':
            # Mac
            chrome = 'open -a /Applications/Google\ Chrome.app %s'
            opera = 'open -a /Applications/Opera.app %s'
        elif system == 'Linux':
            # Linux
            chrome = '/usr/bin/google-chrome %s'
            opera = '/usr/bin/opera %s'
        elif system == 'Windows':
            # Windows
            chrome = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
            opera = 'C:/Program Files (x86)/Opera/Application/opera.exe %s'

        # Add chrome
        paths_chrome = (chrome, 'google-chrome', 'chrome', 'chromium',
            'chromium-browser')
        # Add opera
        paths_opera = (opera, 'opera')

        # Update list browser services
        if(only_chrome is not None):
            self._browsers.append({'name': 'opera', 'service': paths_opera})
        else:
            self._browsers = []

        self._browsers.append({'name': 'chrome', 'service': paths_chrome})

    def execute(self, save_pdf=None):
        print(f'Carico lista url dal file: {self._file_name}')
        # Read links file
        with open(self._file_name, 'r') as myfile:
            url = myfile.read()

            if url:
                self.get_urls(url)
                num_urls = len(self._urls)
                print(f'Link trovati: {num_urls}')
                if(save_pdf is None):
                    self.opens_browsers()
                else:
                    self.save_pdf()
                print('========================')
                print('Operazione completata.')
                print('Bye Bye!!')
            else:
                print('Lista url NON caricata. Spiacente!!')

    def opens_browsers(self):
        services = [x['name'] for x in self._browsers]
        print('========================')
        print(f'Provo ad aprire questi browser: {services}')
        print('========================')

        # For all browsers services open links
        for browser in self._browsers:
            name = browser['name']
            service = browser['service']
            if isinstance(service, str):
                if self.open_new_tab(service, self._urls):
                    print(f'Apertura link con {name}: OK')
            else:
                for version in service:
                    if self.open_new_tab(version, self._urls):
                        print(f'Apertura link con {name}: OK')
                        break

    def get_urls(self, url):
        # Read links in file with regex
        self._urls = re.findall(
            'https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))[^\n]+', url)

    def open_new_tab(self, browser, urls):
        # Open urls in browser service
        check = True
        for link in urls:
            try:
                webbrowser.get(browser).open_new_tab(link)
            except webbrowser.Error as err:
                check = False
                if self._debug:
                    print(f'Errore in browser {browser}. Error: {err}.')

        return check

    def set_remove_path(self, path):
        self._remove_path = path

    def save_pdf(self):
        for link in self._urls:
            file_name = 'pdf/pdf_'+str(link.replace(self._remove_path, ''))+'.pdf'
            pdfkit.from_url(link, file_name)
            print("PDF saved "+link)
