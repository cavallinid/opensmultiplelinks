# Open an urls list in all browsers installed in your pc/mac

## Required
1. Python

## Documentation

### Testare lista di url in tutti i browser presenti sul proprio pc/mac

#### Utilizzo

1. Aprire il file lista.txt contenuto nel progetto e incollarci la lista di url (un link per riga, i link devono iniziare con `http://` o `https://`)
2. Eseguire il comando: `python path/main.py` (_path_ è il percoroso in cui si è salvato il progetto)